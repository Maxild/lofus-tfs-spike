# Note that using the value 4.5.1 also works for projects that target 4.5.2
# (the value 4.5.2 is not recognised by psake)
# This will make msbuild point to one of:
#    [ProgramFilesX86]\MSBuild\12.0\bin\amd64 (x64 bitness of powershell)
#    [ProgramFilesX86]\MSBuild\12.0\bin (x32 bitness of powershell)
# See also http://blogs.msdn.com/b/visualstudio/archive/2013/07/24/msbuild-is-now-part-of-visual-studio.aspx
# See also https://github.com/damianh/psake/commit/0778be759e83014f0bfdd1a0c84caac008c8112f
Framework 4.5.1

# TODO: Create two different 'build number formats' for 'CI' and 'Build' (stable and unstable releases)'
#    $(TeamProject)-$(BuildDefinitionName)-$(year:yyyy)$(Month)$(DayOfMonth): LofusCore-CI-10150417-<counter>
#    $(TeamProject)-$(BuildDefinitionName)-$(Rev:.rr): LofusCore-Build-<counter>
#    $(BuildDefinitionName)-$(Rev:.rr): Build-<counter> => tag = 'build-2966'
# Note: build numbers limited to 65535!!!! => daily builds for almost 180 years!!!
# TODO: Why use environment variable

properties {
    $sln_file = "Lofus-vNext.sln"
    $global:configuration = "Release"
    $framework_dir = Get-FrameworkDirectory
    $srcsrv_dir = "${env:ProgramFiles(x86)}\Windows Kits\8.1\Debuggers\x64\srcsrv"
    $base_dir = resolve-path .
    $packages_dir = "$base_dir\packages"
    $build_dir = "$base_dir\build"
    $source_dir = "$base_dir\src"
    $test_dir = "$base_dir\test"
    $commonAssemblyInfo = "$source_dir\CommonAssemblyInfo.cs"
    $version = "1.0"
    $buildnumber = "13" # => Brf.Lofus.Core.1.0.13-Unstable.nupkg for local builds
}

# TODO: Appveyor build number, or use gitVersion
if($env:TF_BUILD_BUILDNUMBER -ne $null) {
    # TODO: Need to parse it to get the <counter>
    $buildnumber = $env:TF_BUILD_BUILDNUMBER # 2966 => Brf.Lofus.Core.1.0.2966-Unstable.nupkg
}

task default -depends dev

task dev -depends compile, test -description "developer build (before commits)"
task full -depends version, commonAssemblyInfo, dev, srcidx, create_nupkg -description "full build (does not publish nupkg)"
task ci -depends unstable, full -description "ci build"
#task daily -depends unstable, full, publish_nupkg -description "pre-release build of published nupkg"
#task release -depends stable, full, publish_nupkg -description "release build of published nupkg"

task VerifyTools {
    # Visual Studio 2013 will exclusively use 2013 MSBuild and C# compilers (assembly version 12.0)
    # and the 2013 Toolset (ToolsVersion 12.0)
    #$version = &"$framework_dir\MSBuild.exe" /nologo /version
    $version = &{msbuild /nologo /version}
    #$expectedVersion = "4.0.30319.34209" # This is MSBuild version at framework path
    $expectedVersion = "12.0.31101.0"
    Write-Host "Framework directory (GAC) is $framework_dir"
    Write-Host "MSBuild version is $version"
    Assert ($version -eq $expectedVersion) "MSBuild has version '$version'. It should be '$expectedVersion'."
}

task unstable {
    $global:uploadCategory = "Lofus-Unstable"
    $global:uploadMode = "Unstable"
    $global:configuration = "Release"
}

task stable {
    $global:uploadCategory = "Lofus"
    $global:uploadMode = "Stable"
    $global:configuration = "Release"
}

# TODO: Tag in GIT should be build-${buildnumber}
task version {
    $commitId = Get-Git-Commit-Full
    # SemVer: In a nutshell, semantic versions look like Major.Minor.Patch[-PrereleaseToken], such that:
    #               * A change in Major is a breaking change
    #               * A change in Minor adds functionality but is non-breaking
    #               * A change in Patch represents a bug fix (we use build number)
    #               * An optional PrereleaseToken indicates a prerelase of vNext (we use -Unstable).

    # Nuget (SemVer): $version is Major.Minor, and buildnumber is a build number.
    $global:nupkgVersion = "$version.${buildnumber}"

    # Unstable indicates a pre-release, so add (fixed) prerelease token (no alpha1, alpha2, beta1, beta2 etc.)
    if ($global:uploadCategory -and $global:uploadCategory.EndsWith("-Unstable")){
        $global:nupkgVersion += "-Unstable"
    }

    # .NET assembly
    $global:assemblyVersion ="${version}.0"
    $global:assemblyFileVersion ="${version}.${buildnumber}.0"
    $global:assemblyInformationalVersion = "$nupkgVersion / $commitId /"
    # To access version information at run time in .NET:
    #    var executingAssembly = Assembly.GetExecutingAssembly();
    #    var fv = System.Diagnostics.FileVersionInfo.GetVersionInfo(executingAssembly.Location);
    #    Console.WriteLine(executingAssembly.GetName().Version); // AssemblyVersion
    #    Console.WriteLine(fv.FileVersion);                      // AssemblyFileVersion
    #    Console.WriteLine(fv.ProductVersion);                   // AssemblyInformationalVersion
}

task versionInfo -depends unstable, version {
    Write-Host "Version: $nupkgVersion"
    Write-Host "AssemblyVersion: $assemblyVersion"
    Write-Host "AssemblyFileVersion: $assemblyFileVersion"
    Write-Host "AssemblyInformationalVersion: $assemblyInformationalVersion"
}

# Source index the pdb file (See also http://ctaggart.github.io/SourceLink/exe.html)
# SourceLink enables GIT to be the source server by indexing the pdb file with http(s) download links.
task srcidx {
    # This should only be performed on the build server (or in a clean checkout)
    # The SourceLink.exe tool is used to insert (the version control) information
    # into the "srcsrv" stream of the target .pdb file.
    $commitId = Get-Git-Commit-Full
    if ($commitId -ne "0000000000000000000000000000000000000000") {

        # Status should be 'clean'
        $gitStatus = (@(git status --porcelain) | Out-String)
        if ( -not ([string]::IsNullOrWhiteSpace($gitStatus)) ) {
            throw ("Git working tree or Git index is not clean, because 'git status --porcelain' is showing some output!!")
        }

        # URL for downloading the source files, use {0} for commit and %var2% for path
        $commitIdPlaceholder = "{0}"
        $filePathPlaceholder = "%var2%" # %% is escaping because of environment variable syntax

        # TFS-Git (doesn't work????)
        #$repositoryId = "a84385b0-f1d2-4f0a-9c17-fa757c96648a"
        #$repositoryName = "MOM"
        # Web UI download URL
        #$get_file_url = "http://tfs05te:8080/tfs/defaultcollection/$repositoryName/_api/_versioncontrol/itemContent?repositoryId=$repositoryId&path=$filePathPlaceholder&version=GC${commitIdPlaceholder}&contentOnly=false&__v=5"
        # Undocumented VSO REST API for files
        #$get_file_url = "http://tfs05te:8080/tfs/defaultcollection/_apis/git/repositories/$repositoryId/items?api-version=1.0&scopepath=/$filePathPlaceholder&versionType=commit&version=$commitIdPlaceholder"

        # Bitbucket public repo
        $get_file_url = "https://bitbucket.org/Maxild/lofus-tfs-spike/raw/$commitIdPlaceholder/$filePathPlaceholder"

        # Github public repo (https://raw.githubusercontent.com/ctaggart/SourceLink/{0}/%var2%)
        #$get_file_url = "https://raw.githubusercontent.com/maxild/???/$commitIdPlaceholder/$filePathPlaceholder"

        $projectfile   = "$source_dir\Brf.Lofus.Core\Brf.Lofus.Core.csproj"

        exec {
            &"$packages_dir\SourceLink\tools\SourceLink.exe" index `
                -pr $projectfile `
                -pp Configuration $global:configuration `
                -nf "$commonAssemblyInfo" `
                -nf Properties\AssemblyInfo.cs `
                -u $get_file_url `
                -c $commitId
        }

        # Get help with this command...
        #exec { &"$packages_dir\SourceLink\tools\SourceLink.exe" index -h }
    }
}

task srcidxInit {
    $global:pdbfile = "$source_dir\Brf.Lofus.Core\bin\$global:configuration\Brf.Lofus.Core.pdb"
}

task srcidx_dumpbin {
    # The assembly will contain the name of the .pdb file that was created
    # and a GUID for it unless building a .pdb was disabled.
    # You can view the GUID using dumpbin.exe.
    $dllfile = "$source_dir\Brf.Lofus.Core\bin\$global:configuration\Brf.Lofus.Core.dll"
    &"${env:ProgramFiles(x86)}\Microsoft Visual Studio 12.0\VC\bin\dumpbin.exe" /headers $dllfile
}

# The checksums command can be used to print a list of all the index source files and their checksums.
task srcidx_checksums -depends srcidxInit {
    exec {
        Write-Host "print a list of all the index source files and their checksums." -ForegroundColor Yellow
        &"$packages_dir\SourceLink\tools\SourceLink.exe"  checksums -p $pdbfile

        Write-Host "print the URLs instead." -ForegroundColor Yellow
        &"$packages_dir\SourceLink\tools\SourceLink.exe"  checksums -p $pdbfile -nf -u

        Write-Host "check that checksums from downloading the files match what is in the pdb." -ForegroundColor Yellow
        &"$packages_dir\SourceLink\tools\SourceLink.exe"  checksums -p $pdbfile -c
    }
}

# prints the source index of the pdb file (equivalent to pdbstr SDK tool)
task srcidx_pdbstrr -depends srcidxInit {
    exec { &"$packages_dir\SourceLink\tools\SourceLink.exe" pdbstrr -p $pdbfile }
}

# The index can be viewed using a the Windows SDK tool pdbstr
task srcidx_srctoolx -depends srcidxInit {
    exec { &"$packages_dir\SourceLink\tools\SourceLink.exe" srctoolx -p $pdbfile }
}

# This utility is used by the indexing scripts to insert the version
# control information into the "srcsrv" stream of the target .pdb file.
# It can also be used to view the "srcsrv" stream that the pdb file contains.
# You can use this information to verify that the indexing scripts (i.e. SourceLink.exe)
# is working properly.
# Note: The "srcsrv" stream is essentially a text file (script) that
#       describes how to get the source code from a file.
task srcidx_pdbstr -depends srcidxInit {
    &"$srcsrv_dir\pdbstr.exe" -r -p:$pdbfile -s:srcsrv
}

# Dumps source information from a pdb file.
# This utility lists all files indexed within a .pdb file.
# For each file, it lists the raw download URL containing
#    * the full path (%var2%, 49bcace95d09a0b243810c9c94d65fc2714b6ba1),
#    * the full commit id ({0}, src/Brf.Lofus.Core/DateExtensions.cs)
task srcidx_srctool -depends srcidxInit {
    &"$srcsrv_dir\srctool.exe" $pdbfile -x
}

task compile {
    Write-Host "Compiling with '$global:configuration' configuration" -ForegroundColor Yellow

    # Nuget package restore
    exec { &"$base_dir\.nuget\Nuget.exe" restore }
    # compile
    exec { msbuild /t:Clean /t:Build /p:Configuration=$global:configuration /v:q /p:nowarn="1591 1573" /p:VisualStudioVersion=12.0 /maxcpucount "$sln_file" }
}

task test -depends compile {

    $test_prjs = @( `
        "$test_dir\Brf.Lofus.Core.Test\bin\$global:configuration\Brf.Lofus.Core.Test.dll" `
        )

    $xunitConsoleRunner = "$packages_dir\xunit.runner.console.2.0.0\tools\xunit.console.exe"

    $hasErrors = $false
    $test_prjs | ForEach-Object {
        $arguments = "$_"
        Invoke-Expression "$xunitConsoleRunner $arguments"
        if ($lastexitcode -ne 0) {
            $hasErrors = $true
            Write-Host "-------- ---------- -------- ---"
            Write-Host "Failure for $_ - $lastexitcode"
            Write-Host "-------- ---------- -------- ---"
        }
    }
    if ($hasErrors) {
        throw ("Test failure!")
    }
}

task commonAssemblyInfo {
    create-commonAssemblyInfo "$commonAssemblyInfo"
}

task create_nupkg {
    delete_directory "$build_dir"

    $dest_dir = "$build_dir\Brf.Lofus.Core"
    create_directory "$dest_dir\lib\net45"
    copy_files "$source_dir\Brf.Lofus.Core\bin\$global:configuration" "$dest_dir\lib\net45" *.srcsrv
    #copy_files "$source_dir\Brf.Lofus.Core\bin\$global:configuration\aspnet50" "$dest_dir\lib\aspnet50"
    #copy_files "$source_dir\Brf.Lofus.Core\bin\$global:configuration\aspnetcore50" "$dest_dir\lib\aspnetcore50"

    $nuspec = "$dest_dir\Brf.Lofus.Core.nuspec"
    create-nuspec "$nuspec"
    exec { &"$base_dir\.nuget\nuget.exe" pack "$nuspec" -out "$build_dir" }
}

task publish_nupkg {
    # 'nuget.exe setApiKey Your-API-Key' has been invoked prior to running this task (in order not to disclose api-key)
    exec { &"$base_dir\.nuget\nuget.exe" push "$build_dir\Brf.Lofus.Core.$nupkgVersion.nupkg" }
    # TODO: Access is denied
    #copy_files "$build_dir\Brf.Lofus.Core.$nupkgVersion.nupkg" "\\tfs11pr\Packages"
    #cp "$build_dir\Brf.Lofus.Core.$nupkgVersion.nupkg" "\\tfs11pr\Packages"
}

# TODO: Denne skal ændres til nye build definitioner
task tb -depends teambuild
task teambuild -description "Start team build (Head_Lofus)" {
  # start team build on the commandline in new powershell window
  start-process powershell.exe -argument '-nologo -noprofile -executionpolicy bypass -command &"${Env:ProgramFiles(x86)}\Microsoft` Visual` Studio` 12.0\Common7\IDE\TFSBuild.exe" start http://tfs01pr:8080/tfs/brfkredit Application.Laaneberegninger Head_Lofus; if ($LastExitCode -ne 0) { Write-Host "ERROR" -ForegroundColor Red -NoNewline; Write-Host ": The build did not succeed."; Read-Host "Press any key..."  }'
}

# -------------------------------------------------------------------------------------------------------------
# generalized functions
# --------------------------------------------------------------------------------------------------------------
function Get-File-Exists-On-Path([string]$file)
{
    $results = ($env:Path).Split(";") | Get-ChildItem -filter $file -erroraction silentlycontinue
    $found = ($results -ne $null)
    return $found
}

function Is-Cwd-Git-Repo {
    if ((Test-Path ".git") -eq $TRUE) {
        return $TRUE
    }

    # Test within parent dirs
    $checkIn = (Get-Item .).parent
    while ($checkIn -ne $NULL) {
        $pathToTest = $checkIn.fullname + '/.git'
        if ((Test-Path $pathToTest) -eq $TRUE) {
            return $TRUE
        } else {
            $checkIn = $checkIn.parent
        }
    }

    return $FALSE
}

# partial sha with 7 chars (a3497c9)
function Get-Git-Commit
{
    if ((Get-File-Exists-On-Path "git.exe") -and (Is-Cwd-Git-Repo)) {
        $gitLog = git log --oneline -1
        return $gitLog.Split(' ')[0]
    }
    else {
        return "0000000"
    }
}

# full sha with 40 chars (a3497c9f044f45b5e295f7fb9d7494df3c209a31)
function Get-Git-Commit-Full
{
    if ((Get-File-Exists-On-Path "git.exe") -and (Is-Cwd-Git-Repo)) {
        $gitLog = git log -1 --format=%H
        return $gitLog;
    }
    else {
        return "0000000000000000000000000000000000000000"
    }
}

function Get-PackagePath {
    param([string]$packageName)

    $packagePath = Get-ChildItem "$packages_dir\$packageName.*" |
                        Sort-Object Name -Descending |
                        Select-Object -First 1
    Return "$packagePath"
}

function Get-DependencyPackageFiles
{
    param([string]$packageName, [string]$frameworkVersion = "net45")

    $packagePath = Get-PackagePath $packageName
    Return "$packagePath\lib\$frameworkVersion\*"
}

# directory where MSBuild.exe is to be found
function Get-FrameworkDirectory
{
    $frameworkPath = "$env:windir\Microsoft.NET\Framework\v4.0*"
    $frameworkPathDir = ls "$frameworkPath"
    if ( $frameworkPathDir -eq $null ) {
        throw "Building Brf.Lofus.Core requires .NET 4.0, which doesn't appear to be installed on this machine"
    }
    $net4Version = $frameworkPathDir.Name
    return "$env:windir\Microsoft.NET\Framework\$net4Version"
}

function global:delete_directory($directory_name)
{
  rd $directory_name -recurse -force  -ErrorAction SilentlyContinue | out-null
}

function global:delete_file($file)
{
    if ($file) {
        remove-item $file  -force  -ErrorAction SilentlyContinue | out-null
    }
}

function global:create_directory($directory_name)
{
  mkdir $directory_name  -ErrorAction SilentlyContinue  | out-null
}

function global:copy_files($source, $destination, $exclude = @()) {
    create_directory $destination
    Get-ChildItem $source -Recurse -Exclude $exclude |
        Copy-Item -Destination {Join-Path $destination $_.FullName.Substring($source.length)}
}

function global:create-commonAssemblyInfo($filename)
{
    $date = Get-Date
    "using System;
using System.Reflection;
using System.Runtime.InteropServices;

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4927
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: AssemblyCompany(""BRFkredit a/s"")]
[assembly: AssemblyCopyright(""Copyright BRFkredit a/s 2002-" + $date.Year + ". All rights reserved."")]
[assembly: AssemblyTrademark("""")]
[assembly: ComVisible(false)]

[assembly: AssemblyTitle(""Lofus"")]
[assembly: AssemblyVersion(""$assemblyVersion"")]
[assembly: AssemblyFileVersion(""$assemblyFileVersion"")]
[assembly: AssemblyInformationalVersion(""$assemblyInformationalVersion"")]
[assembly: AssemblyProduct(""Lofus"")]
[assembly: AssemblyDescription(""Lofus is a calculation engine to perform various calculations on mortgage and bank products in .NET."")]

[assembly: CLSCompliant(true)]

#if DEBUG
[assembly: AssemblyConfiguration(""Debug"")]
#else
[assembly: AssemblyConfiguration(""Release"")]
#endif
" | out-file $filename -encoding "utf8"
}

function global:create-nuspec($path)
{
    $date = Get-Date
    "<?xml version=""1.0""?>
<package xmlns=""http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd"">
  <metadata>
    <id>Brf.Lofus.Core</id>
    <version>$nupkgVersion</version>
    <authors>Morten Maxild</authors>
    <owners>Morten Maxild</owners>
    <requireLicenseAcceptance>false</requireLicenseAcceptance>
    <summary>This package includes the Core library for Lofus.</summary>
    <description>This package includes the Core library for Lofus. Lofus is a calculation engine to perform various calculations on mortgage and bank products in .NET. </description>
    <copyright>Copyright BRFkredit a/s 2002-" + $date.Year + ". All rights reserved.</copyright>
  </metadata>
</package>" | out-file $path -encoding "utf8"
}
