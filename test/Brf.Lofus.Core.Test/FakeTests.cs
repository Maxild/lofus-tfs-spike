using Xunit;

namespace Brf.Lofus.Core.Test
{
    public class FakeTests
    {
        [Fact]
        public void Test()
        {
            Assert.Equal(2, 2);
        }
    }

    public class MathTests
    {
        [Fact]
        public void Test()
        {
            Assert.Equal(3m, Math.Add(1, 2));
        }
    }
}
