using System;

namespace Brf.Lofus.Core
{
    public static class DateExtensions
    {
        public static bool IsLeapYear(this DateTime d)
        {
            return DateTime.IsLeapYear(d.Year);
        }
    }
}
